require('dotenv').config();
const { TraceUtils } = require('@themost/common');
const {JsonLogger} = require('@themost/json-logger');
TraceUtils.useLogger(new JsonLogger({
    format: 'raw'
}));
