import { DataError, DataObjectState } from '@themost/data'

/**
 * 
 * @param {import('@themost/data').DataEventArgs} event
 * @returns Promise<void>
 */
async function beforeSaveAsync(event) {
    // get scholarship in order to find target form
    const context = event.model.context;
    let scholarship = null;
    if (Object.prototype.hasOwnProperty.call(event.target, 'form') === false) {
        return;
    }
    if (event.state === DataObjectState.Insert) {
        scholarship = await context.model('Scholarship').find(event.target.scholarship).expand('event').getTypedItem();
    } else {
        /**
         * @type {DataObject}
         */
        const target = await event.model.find(event.target).getTypedItem();
        if (target == null) {
            throw new DataError('The target object cannot be found or is inaccessible');
        }
        scholarship = await target.property('scholarship').expand('event').getTypedItem();
    }
    if (scholarship == null) {
        throw new DataError('The target scholarship cannot be found or is inaccessible');
    }
    // get scholarship form
    const scholarshipForm = await scholarship.getForm();
    if (scholarshipForm == null) {
        throw new DataError('The scholarship form cannot be found or is inaccessible');
    }
    // get the model which represents a scholarship application
    const name = scholarshipForm.properties.name;
    const model = context.model(name);
    // save application form
    event.target.form.scholarship = scholarship.id;
    await model.save(event.target.form);
}

function beforeSave(event, callback) {
    void beforeSaveAsync(event).then(() => callback()).catch((err) => callback(err))
}

export {
    beforeSave
}