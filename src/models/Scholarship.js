import { EdmMapping, EdmType, DataObject, DataConfigurationStrategy, ODataModelBuilder } from '@themost/data';
import { JsonFormInspector } from '@themost/form-inspector';
import {lt} from 'semver';
import util from 'util';
import fs from 'fs';
import {HttpNotAcceptableError, HttpNotFoundError, HttpServerError, TraceUtils} from '@themost/common';
import { promisify } from 'util';

/**
 * @property {import('@themost/express').HttpContext} context
 */
@EdmMapping.entityType('Scholarship')
class Scholarship extends DataObject {
    constructor() {
        super();
    }
    @EdmMapping.func('Form.json', 'Object')
    async getForm() {
        /**
         * @type {{id: number, formType: string, version: string}}
         */
        const form = await this.property('form').getItem();
        if (form == null) {
            throw new HttpNotFoundError('Scholarship application form cannot be found');
        }
        // noinspection JSUnresolvedReference
        /**
         * @type {import('@themost/express').ExpressDataApplication}
         */
        const application = this.context.getApplication();
        // noinspection JSCheckFunctionSignatures
        const service = application.getService(function PrivateContentService(){});
        /**
         * @type {string}
         */
        const formPath = await new Promise((resolve, reject) => {
            service.resolvePhysicalPath(this.context, form, (err, physicalPath) => {
                if (err) {
                    return reject(err);
                }
                return resolve(physicalPath);
            })
        });
        const readFileAsync = util.promisify(fs.readFile);
        let content = await readFileAsync(formPath);
        if (content == null) {
            throw new HttpNotFoundError('Evaluation form cannot be found');
        }
        let buffer = Buffer.from(content,'base64');
        let result;
        result = JSON.parse(buffer.toString('utf8'));

        try {
            result.properties = result.properties || {};
            Object.assign(result.properties, {
                name: form.formType,
                _id: form.id,
                _scholarship: this.id
            });
            // post-processing
            let shouldUpgrade = true;
            let model = this.context.model(form.formType);
            if (model != null) {
                shouldUpgrade = lt(model.version, form.version)
            }
            if (shouldUpgrade) {
                // load for inspector
                const inspector = new JsonFormInspector();
                // add inheritance
                result.properties.inherits = 'RequestScholarshipFormItem'
                model = inspector.inspect(result);
                // post-processing steps
                Object.assign(model, {
                    version: model.version ,// set version (follow updates)
                    hidden: true
                });
                // add event field for further processing of evaluation results
                model.fields.push.apply(model.fields, [
                    {
                        name: 'scholarship',
                        type: this.getModel().name,
                        editable: false,
                        nullable: false,
                        indexed: true,
                    }
                ]);
                // reset privileges
                /**
                 * @type {import('@themost/data').DataConfigurationStrategy}
                 */
                const dataConfiguration = application
                    .getConfiguration().getStrategy(DataConfigurationStrategy);

                model.privileges.splice(0);

                model.privileges.push.apply(model.privileges, [
                    {
                        'mask': 15,
                        'type': 'global'
                    },
                    {
                        'mask': 1,
                        'type': 'global',
                        'account': 'Administrators'
                    },
                    {
                        "mask": 15,
                        "type": "self",
                        "account": "Candidates",
                        "filter": "createdBy eq me()",
                        "scope": [
                            "students"
                        ]
                    },
                    {
                        "mask": 15,
                        "type": "self",
                        "account": "Students",
                        "filter": "createdBy eq me()",
                        "scope": [
                            "students"
                        ]
                    }
                ]);
                dataConfiguration.setModelDefinition(model);
                // add entity set
                const builder = application.getService(ODataModelBuilder);
                builder.addEntitySet(model.name, model.name);
                if (model.hidden) {
                    builder.removeEntitySet(model.name);
                }
                // update version
                form.version= model.version;
                await this.context.model('RequestScholarshipForm').silent().save(form);
            }
            return result;
        } catch (err) {
            TraceUtils.error(`Error while decoding application form ${form.id} content.`);
            TraceUtils.error(err);
            throw new HttpServerError('An error occurred while decoding application form.');
        }
    }

    @EdmMapping.param('attributes', 'Object', true)
    @EdmMapping.param('file', EdmType.EdmStream, false)
    @EdmMapping.action('Form', 'Object') 
    async setForm(file, attributes){
        const service = this.context.getApplication().getService(function PrivateContentService() {});
        if (service == null) {
            throw new Error('Content service cannot be found or is inaccessible');
        }
        if (file.contentType !== 'application/json') {
            throw new HttpNotAcceptableError('Expected a valid json content');
        }
        const {id} = this; 
        const form = Object.assign({
            version: '1.0.0'
        }, attributes, {
            contentType: 'application/json',
            additionalType: 'RequestScholarshipForm',
            scholarship: id
        });
        const copyFromAsync = promisify(service.copyFrom).bind(service);
        await copyFromAsync(this.context, file.path, form); 
        await this.context.model('Scholarship').silent().save({
            id,
            form
        });
        return form;
    }

    @EdmMapping.func('FormItems', EdmType.CollectionOf('Object'))
    async getFormItems() {
        const form = await this.getForm();
        return this.context.model(form.properties.name).asQueryable().prepare();
    }

    @EdmMapping.param('itemOrItems', 'RequestScholarshipFormItem', false, true)
    @EdmMapping.action('FormItems', 'RequestScholarshipFormItem')
    async setFormItems(itemOrItems) {
        const form = await this.getForm();
        return this.context.model(form.properties.name).save(itemOrItems);
    }


}

export {
    Scholarship
}
