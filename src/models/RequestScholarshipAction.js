import EnableAttachmentModel from './EnableAttachmentModel';
import { EdmType, EdmMapping } from '@themost/data';

@EdmMapping.entityType('RequestScholarshipAction')
class RequestScholarshipAction extends EnableAttachmentModel {
    constructor() {
        super();
    }

    /**
     * Adds an attachment
     * @param {*} file
     * @param {*=} extraAttributes
     */
     @EdmMapping.param('extraAttributes', 'Object', true, true)
     @EdmMapping.param('file', EdmType.EdmStream, false)
     @EdmMapping.action('AddAttachment', 'Attachment')
     async addAttachment(file, extraAttributes) {
         const attachment = Object.assign({
             name: file.contentFileName
         }, file, extraAttributes);
         return await super.addAttachment(attachment);
     }
     /**
      * Removes an attachment
      * @param {*} attachment
      */
     @EdmMapping.param('attachment', 'Attachment', true, true)
     @EdmMapping.action('RemoveAttachment', 'Attachment')
     async removeAttachment(attachment) {
         return await super.removeAttachment(attachment.id);
     }

     @EdmMapping.func('Attachments', EdmType.CollectionOf('Attachment'))
      async getAttachments() {
          return this.property('attachments').prepare();
      }

    /**
     * Gets item review
     */
    @EdmMapping.func('review', 'RequestScholarshipReview')
    getReview() {
        return this.context.model('RequestScholarshipReview')
            .where('itemReviewed').equal(this.id).prepare();
    }

    /**
     * Set item review
     * @param {*} item
     */
    @EdmMapping.param('item', 'RequestScholarshipReview', true, true)
    @EdmMapping.action('review', 'RequestScholarshipReview')
    async setReview(item) {
        const RequestScholarshipReviews = this.context.model('RequestScholarshipReview');
        // infer object state
        const currentReview = await RequestScholarshipReviews.where('itemReviewed').equal(this.id).getItem();
        if (currentReview == null) {
            if (item == null) {
                return;
            }
            // a new item is going to be inserted
            delete item.id;
            // set reviewed item
            item.itemReviewed = this.id;
        } else {
            if (item == null) {
                // delete review
                RequestScholarshipReviews.remove(currentReview);
            }
            // up
            item.id = currentReview.id;
            // set reviewed item
            item.itemReviewed = this.id;
        }
        return RequestScholarshipReviews.save(item);
    }

}

export {
    RequestScholarshipAction
}