import {DataObject} from '@themost/data';
import {ExpressDataContext} from '@themost/express';

export declare class Scholarship extends DataObject {
    constructor();
    getForm(): Promise<any>;
    getApplications(): Promise<any>;
}
