import { EdmMapping, DataObject } from '@themost/data';

/**
 * @property {import('@themost/express').HttpContext} context
 */
@EdmMapping.entityType('RequestScholarshipEvent')
class ScholarshipEvent extends DataObject {
    constructor() {
        super();
    }
}

export {
    ScholarshipEvent
}
