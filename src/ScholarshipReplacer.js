/* eslint-disable quotes */
import { ApplicationService } from '@themost/common';
import {ModelClassLoaderStrategy, SchemaLoaderStrategy} from '@themost/data';
import { Scholarship } from './models/Scholarship';

class ScholarshipReplacer extends ApplicationService {
    constructor(app) {
        super(app);
    }

    apply() {
        // get schema loader
        const schemaLoader = this.getApplication().getConfiguration().getStrategy(SchemaLoaderStrategy);
        // get model definition
        const model = schemaLoader.getModelDefinition('Scholarship');
        const addFields = [{
            "name": "form",
            "type": "RequestScholarshipForm",
            "many": true,
            "multiplicity": "ZeroOrOne",
            "mapping": {
                "associationType": "association",
                "cascade": "delete",
                "parentModel": "Scholarship",
                "parentField": "id",
                "childModel": "RequestScholarshipForm",
                "childField": "scholarship",
            }
        }, {
            "name": "event",
            "type": "ScholarshipEvent",
            "many": true,
            "nested": true,
            "multiplicity": "ZeroOrOne",
            "mapping": {
                "associationType": "association",
                "cascade": "delete",
                "parentModel": "Scholarship",
                "parentField": "id",
                "childModel": "ScholarshipEvent",
                "childField": "scholarship"
            }
        }];

        addFields.forEach((field) => {
            let findAttribute = model.fields.find((item) => {
                return item.name === field.name;
            });
            if (findAttribute == null) {
                model.fields.push(field);
            }
        });

        // update model definition
        schemaLoader.setModelDefinition(model);
        // get model class
        const loader = this.getApplication().getConfiguration().getStrategy(ModelClassLoaderStrategy);
        const ScholarshipClass = loader.resolve(model);
        // extend model
        const { getForm, setForm, getFormItems, setFormItems } = Scholarship.prototype;
        Object.assign(ScholarshipClass.prototype, {
            getForm,
            setForm,
            getFormItems,
            setFormItems
        });
    }
}

export {
    ScholarshipReplacer
}
