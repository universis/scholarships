/* eslint-disable quotes */
import { ApplicationService } from '@themost/common';
import {SchemaLoaderStrategy} from '@themost/data';

class StudyProgramReplacer extends ApplicationService {
    constructor(app) {
        super(app);
    }

    apply() {
        // get schema loader
        const schemaLoader = this.getApplication().getConfiguration().getStrategy(SchemaLoaderStrategy);
        // get model definition
        const model = schemaLoader.getModelDefinition('StudyProgram');
        const findAttribute = model.fields.find((field) => {
            return field.name === 'scholarships'
        });
        if (findAttribute == null) {
            /* eslint-disable quotes */
            model.fields.push({
                "name": "scholarships",
                "type": "Scholarship",
                "many": true,
                "enumerable": true,
                "mapping": {
                    "associationType": "junction",
                    "associationAdapter": "StudyProgramScholarships",
                    "associationObjectField": "studyProgram",
                    "associationValueField": "scholarship",
                    "cascade": "delete",
                    "parentModel": "StudyProgram",
                    "parentField": "id",
                    "childModel": "Scholarship",
                    "childField": "id",
                    "privileges": [
                        {
                            "mask": 15,
                            "type": "global"
                        },
                        {
                            "mask": 1,
                            "type": "global",
                            "account": "*"
                        },
                        {
                            "mask": 15,
                            "type": "global",
                            "account": "Administrators"
                        },
                        {
                            "mask": 15,
                            "type": "global",
                            "filter": "scolarship/department eq departments()",
                            "account": "Registrar"
                        }
                    ]
                }
            });
            /* eslint-enable quotes */
        }
        // update model definition
        schemaLoader.setModelDefinition(model);
    }
}

export {
    StudyProgramReplacer
}
