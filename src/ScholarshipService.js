import { ApplicationService } from '@themost/common';
import { ODataModelBuilder } from '@themost/data';
import LocalScopeAccessConfiguration from './config/scope.access.json';
import {ScholarshipReplacer} from './ScholarshipReplacer';
import {StudyProgramReplacer} from './StudyProgramReplacer';
class ScholarshipService extends ApplicationService {
    constructor(app) {
        super(app);
        // extend universis api scope access configuration
        if (app && app.container) {
            app.container.subscribe((container) => {
                if (container) {
                    // add extra scope access elements
                    const scopeAccess = app.getConfiguration().getStrategy(function ScopeAccessConfiguration() { });
                    if (scopeAccess != null) {
                        scopeAccess.elements.push.apply(scopeAccess.elements, LocalScopeAccessConfiguration);
                    }
                }
            });
            // schema replacers
            new ScholarshipReplacer(app).apply();
            new StudyProgramReplacer(app).apply();
            /**
             * get builder
             * @type ODataModelBuilder
             */
            const builder = app.getService(ODataModelBuilder);
            if (builder != null) {
                // cleanup builder and wait for next call
                builder.clean(true);
                builder.initializeSync();
            }
        }
    }
}

export {
    ScholarshipService
};
