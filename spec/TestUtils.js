import {DataCacheStrategy} from '@themost/data';
import {ServerSentEventService} from '@universis/sse';

/**
 *
 * @param {import('@themost/express').ExpressDataApplication} app
 */
async function finalizeApplicationAsync(app) {
    /**
     * @type {*}
     */
    let service = app.getConfiguration().getStrategy(DataCacheStrategy);
    if (typeof service.finalize === 'function') {
        await service.finalize();
    }
    service = app.getService(ServerSentEventService);
    if (service != null && typeof service.finalize === 'function') {
        service.finalize();
    }
}

export {
    finalizeApplicationAsync
}
