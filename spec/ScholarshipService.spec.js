import { ScholarshipService, ScholarshipSchemaLoader } from '../src/index';
import { finalizeApplicationAsync } from './TestUtils';
import { DataConfigurationStrategy, SchemaLoaderStrategy } from '@themost/data';
import path from 'path';
import { promisify } from 'util';

async function createScholarshipWithEvent(context) {
  const department = await context.model('LocalDepartment').asQueryable().silent().getItem();
  const { currentYear: year } = department;
  const event = {
    name: 'Test Scholarship Event',
    validFrom: new Date(),
    validThrough: new Date()
  }
  const scholarship = {
    name: 'Test Scholarship',
    department,
    year,
    event
  };
  await context.model('Scholarship').silent().save(scholarship);
  const scholarshipEvent = await context.model('ScholarshipEvent').silent().where('id').equal(event.id).getTypedItem();
  expect(scholarshipEvent).toBeTruthy();

  class PrivateContentService {
    constructor() {
      //
    }
  }
  const service = context.getApplication().getService(PrivateContentService);
  const jsonForm = path.resolve(__dirname, 'form.json');
  const copyFromAsync = promisify(service.copyFrom).bind(service);
  const newForm = {
    name: 'Test Scholarship Event',
    contentType: 'application/json',
    additionalType: 'RequestScholarshipForm',
    version: '1.0.0',
    scholarship: scholarship.id
  };
  await copyFromAsync(context, jsonForm, newForm);
  expect(newForm).toBeTruthy();
  // update event
  scholarship.form = newForm;
  await context.model('Scholarship').silent().save(scholarship);
  return scholarship;
}

describe('ScholarshipService', () => {
  /**
   * @type {import('@themost/express').ExpressDataContext}
   */
  let context = null;
  let executeInTransaction = null;
  beforeAll(() => {
    // noinspection NpmUsedModulesInstalled
    const container = require('@universis/api/dist/server/app');
    // noinspection NpmUsedModulesInstalled
    const { TestUtils } = require('@universis/api/dist/server/utils');
    executeInTransaction = TestUtils.executeInTransaction;
    // 1. use service
    /**
     * @type {import('@themost/express').ExpressDataApplication}
     */
    const app = container.get('ExpressDataApplication');
    app.useService(ScholarshipService);
    // 2. add schema loader
    // noinspection JSValidateTypes
    /**
     * @type {import('@themost/data').DefaultSchemaLoaderStrategy}
     */
    const schema = app.getConfiguration().getStrategy(SchemaLoaderStrategy);
    schema.loaders.push(new ScholarshipSchemaLoader(app.getConfiguration()));
    // 3. resolve test database paths
    const dataConfiguration = app.getConfiguration().getStrategy(DataConfigurationStrategy);
    const adapter = dataConfiguration.adapters.find(adapter => adapter.default);
    if (adapter.invariantName === 'sqlite') {
      // noinspection NpmUsedModulesInstalled
      const appModulePath = require.resolve('@universis/api/dist/server/app');
      adapter.options.database = path.resolve(appModulePath, '../../..', adapter.options.database);
    }
    // create context
    context = app.createContext();
  });
  afterAll(async () => {
    if (context) {
      await finalizeApplicationAsync(context.application);
      await context.finalizeAsync();
    }
  });
  afterEach(async () => {
    delete context.user;
  });
  it('should use service', async () => {
    const model = context.model('ScholarshipEvent');
    expect(model).toBeTruthy();
  });

  it('should create scholarship', async () => {
    await executeInTransaction(context, async () => {
      const department = await context.model('LocalDepartment').asQueryable().silent().getItem();
      const { currentYear: year } = department;
      const scholarship = {
        name: 'Test Scholarship',
        department,
        year
      };
      await context.model('Scholarship').silent().save(scholarship);
      expect(scholarship.id).toBeTruthy();
    });
  });

  it('should create scholarship event', async () => {
    await executeInTransaction(context, async () => {
      context.user = {
        name: 'staff1@example.com'
      };
      const newScholarship = await createScholarshipWithEvent(context);
      expect(newScholarship).toBeTruthy();
    });
  });

  it('should get scholarship form items', async () => {
    await executeInTransaction(context, async () => {
      context.user = {
        name: 'staff1@example.com'
      };
      const newScholarship = await createScholarshipWithEvent(context);
      const scholarship = await context.model('Scholarship').where('id').equal(newScholarship.id).getTypedItem();
      const items = await scholarship.getFormItems();
      expect(items).toBeTruthy();
    });
  });
});
